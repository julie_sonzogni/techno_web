import React from 'react';
import { Share, Button, TouchableOpacity, StyleSheet, View, Text, Linking } from 'react-native';



const Medecin = ({ medecin }) => {
    if (!medecin) {
        return null;
    }
    return (
        <View
            style={{
                margin: 20,
                backgroundColor: 'white',
                borderRadius: 20,
                padding: 35,
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
            }}>
            <Text
                style={{
                    marginBottom: 15,
                    fontSize: 24,
                    fontWeight: 'bold',
                    textAlign: 'center',
                }}>
                {medecin.prenom} {medecin.nom}
            </Text>
            <Text style={{ marginBottom: 15, textAlign: 'center', fontStyle: 'italic' }}>
                {medecin.specialite}
            </Text>
            <Text style={styles.horaireTitle}>Horaires : </Text>
            <Text style={styles.horaire}>Lundi : {medecin.lundi}</Text>
            <Text style={styles.horaire}>Mardi : {medecin.mardi}</Text>
            <Text style={styles.horaire}>Mercredi : {medecin.mercredi}</Text>
            <Text style={styles.horaire}>Jeudi : {medecin.jeudi}</Text>
            <Text style={styles.horaire}>Vendredi : {medecin.vendredi}</Text>
            <Text style={styles.horaire}>Samedi : {medecin.samedi}</Text>
            <Text style={styles.horaire}>Dimanche : {medecin.dimanche}</Text>

            <TouchableOpacity
                onPress={() => Linking.openURL(`tel:${medecin.numero_tel}`)}>
                <Text style={{ color: 'blue' }}>{medecin.numero_tel}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Linking.openURL(medecin.lien_maps)}>
                <Text style={{ color: 'blue' }}>{medecin.adresse} {"\n"}</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => Share.share({ message: "Salut ! Je te partage le nom d'un " + medecin.specialite + " à Bruz : " + medecin.prenom + " " + medecin.nom + " ☎️ : " + medecin.numero_tel })}>
                <Text style={{ color: 'blue' }}>Partager avec quelqu'un</Text>
            </TouchableOpacity>



        </View>


    );
};

var styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 25,
        width: '98%',
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: '#CDD2E1',
    },
    backgroundImage: {
        width: 320,
        height: 480,
    },
    horaire: {
        marginBottom: 15,
        textAlign: 'center',
    },
    horaireTitle: {
        marginBottom: 15,
        textAlign: 'center',
        fontWeight: 'bold',
        textDecorationLine: 'underline',
    },
});
export default Medecin;