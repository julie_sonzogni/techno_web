import React from 'react';
import practicien from "../practicien.json";
import Medecin from "../components/Medecin";
import { Modal, TouchableOpacity, StyleSheet, View, Text, FlatList, Pressable, Alert } from 'react-native';


class Specialite extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      specialiteSelected: null,
    };
  }

  static navigationOptions = {
    title: 'Home',
    header: null
  };

  openSpecialite = (spe) => {
    this.setState({ specialiteSelected: spe })
  };

  closeSpecialite = () => {
    this.setState({ specialiteSelected: null })
  }


  render() {
    const { } = this.state;
    return (
      <>
        <PopupModal spe={this.state.specialiteSelected} onclosed={this.closeSpecialite} />

        <View
          style={{
            flex: 1,
            padding: 25,
            width: '98%',
            alignSelf: 'center',
            backgroundColor: '#CED0CE',
            justifyContent: 'center',
          }}>

          <Text style={styles.txt}>Clique sur la spécialité : {"\n"}</Text>

          <TouchableOpacity style={styles.button}
            onPress={() => {
              this.openSpecialite("Medecin");
            }
            }
          >
            <Text style={{ color: 'white' }}>Médecin</Text>
          </TouchableOpacity>

          <Text>{"\n"}</Text>

          <TouchableOpacity style={styles.button}
            onPress={() => {
              this.openSpecialite("Sage-femme");
            }
            }
          >
            <Text style={{ color: 'white' }}>Sage-femme</Text>
          </TouchableOpacity>

          <Text>{"\n"}</Text>

          <TouchableOpacity style={styles.button}
            onPress={() => {
              this.openSpecialite("Dentiste");
            }
            }
          >
            <Text style={{ color: 'white' }}>Dentiste</Text>
          </TouchableOpacity>

          <Text>{"\n"}</Text>

          <TouchableOpacity style={styles.button}
            onPress={() => {
              this.openSpecialite("Podologue");
            }
            }
          >
            <Text style={{ color: 'white' }}>Podologue</Text>
          </TouchableOpacity>

          <Text>{"\n"}</Text>

          <TouchableOpacity style={styles.button}
            onPress={() => {
              this.openSpecialite("Kinésithérapeute");
            }
            }
          >
            <Text style={{ color: 'white' }}>Kinésithérapeute</Text>
          </TouchableOpacity>


        </View>
      </>
    );
  }
}

const PopupModal = ({ spe, onclosed }) => {
  console.log(spe);
  if (!spe) {
    return null;
  }
  const practiSpe = practicien.filter(item => {
    return item.specialite === spe;
  });

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={Boolean(spe)}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onclosed();

      }}>

      <FlatList
        data={practiSpe}
        renderItem={({ item }) => (
          <Medecin
            medecin={item}
          />
        )}
        keyExtractor={item => item.id}
      />


      <Pressable
        style={[
          { borderRadius: 20, padding: 10, elevation: 2 },
          { backgroundColor: '#dcab00' },
          { justifyContent: 'center' },
        ]}
        onPress={() => onclosed()}>
        <Text style={{ textAlign: 'center' }}>Quitter</Text>
      </Pressable>

    </Modal>
  );
};

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  txt: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: "center",
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#68656e',
    padding: 10,
    borderRadius: 5,
  }

});

export default Specialite;