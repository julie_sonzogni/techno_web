import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './Home';
import MedecinScreen from './Medecin';
import Specialite from './Specialite';
import { Vibration } from "react-native";

const Tab = createBottomTabNavigator();

const vibratoOnTapPress = () => ({
  tabPress: () => {
    Vibration.vibrate();
  },
}
);



const MyTheme = {
  dark: false,
  colors: {
    primary: 'rgb(147, 0, 147)',
    card: 'rgb(190, 190, 190)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(190, 190, 190)',
    notification: 'rgb(35, 36, 107)',
  },
};
const Navig = () => {

  return (
    <NavigationContainer theme={MyTheme}>
      <Tab.Navigator>
        <Tab.Screen
          name="Home"
          component={HomeScreen}

          options={{
            tabBarLabel: 'Home'
          }}
          listeners={vibratoOnTapPress}

        />
        <Tab.Screen
          name="Medecin"
          component={MedecinScreen}
          options={{
            tabBarLabel: 'Médecin'
          }}

          listeners={vibratoOnTapPress}
        />
        <Tab.Screen
          name="Specialite"
          component={Specialite}
          options={{
            tabBarLabel: 'Spécialité'
          }}
          listeners={vibratoOnTapPress}
        />

      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default Navig;
