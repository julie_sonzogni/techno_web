import React from 'react';
import practicien from '../practicien.json';
import Medecin from '../components/Medecin';
import {
  Alert,
  Modal,
  Pressable,
  FlatList,
  StyleSheet,
  View,
  Text,
  TextInput,
} from 'react-native';

class MedecinScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      currentItem: undefined,
      value: '',
    };
    this.arrayNew = practicien;
  }

  static navigationOptions = {
    title: 'Home',
    header: null,
  };
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#CED0CE',
          justifyContent: 'center',
        }}
      />
    );
  };

  searchItems = text => {
    let newData = this.arrayNew.filter(item => {
      const nomPrenom = item.nom + ' ' + item.prenom;
      const itemData = `${nomPrenom.toUpperCase()}`;
      const textData = text.toUpperCase();
      if (text.length > 0) {
        return itemData.indexOf(textData) > -1;
      }
    });
    this.setState({
      data: newData,
      value: text,
    });
  };

  renderHeader = () => {
    return (
      <TextInput
        style={{
          height: 60,
          borderRadius: 20,
          borderColor: '#000',
          borderWidth: 1,
        }}
        placeholder="   Prénom Nom"
        onChangeText={text => this.searchItems(text)}
        value={this.state.value}
      />
    );
  };
  setCurrentItem = item => {
    this.setState({ currentItem: item });
  };
  onClosedModal = () => {
    this.setCurrentItem(null);
  }
  render() {
    const { currentItem } = this.state;
    return (
      <View style={styles.container}>
        <PopupModal item={currentItem} onclosed={this.onClosedModal} />

        <Text style={styles.txt}>Rentre le prénom et nom du praticien {'\n'}</Text>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <View>
              <Pressable
                style={[
                  { padding: 10, elevation: 2 },
                  { backgroundColor: 'white' },
                ]}
                onPress={() => this.setCurrentItem(item)}>
                <Text>
                  {item.prenom} {item.nom} : {item.specialite}{' '}
                </Text>
              </Pressable>

            </View>
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />

      </View>
    );
  }
}

const PopupModal = ({ item, onclosed }) => {
  console.log(item);
  if (!item) {
    return null;
  }
  return (

    <Modal
      animationType="slide"
      transparent={true}
      visible={Boolean(item)}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
        onclosed();

      }}>
      <Medecin
        medecin={item}
      />
      <Pressable
        style={[
          { borderRadius: 20, padding: 10, elevation: 2 },
          { backgroundColor: '#dcab00' },
          { justifyContent: 'center' },
        ]}
        onPress={() => onclosed()}>
        <Text style={{ textAlign: 'center' }}>Quitter</Text>
      </Pressable>

    </Modal>
  );
};

var styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    width: '98%',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#CDD2E1',
  },
  icone: {
    width: 40,
    height: 40,
    padding: 10,
    margin: 5,
    resizeMode: 'stretch',
  },
  txt: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: "center",
  },
});

export default MedecinScreen;
