import { View, Text, TouchableOpacity, Image, StyleSheet, ImageBackground } from 'react-native';
import React from 'react';
function HomeScreen({ navigation }) {
    return (
        <ImageBackground
            source={require("../fond_ecran.png")}
            style={styles.container}

        >

            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.titre}>{"\n"}Bienvenue sur la platforme de recherche de praticien {"\n"}</Text>

                <Image source={require('../logo.png')}
                    style={styles.logoBruz}
                />
                <Text style={styles.textLieu}>{"\n"} Lieu : Bruz 📍 {"\n"}</Text>

                <Text>{"\n"}</Text>
                <TouchableOpacity style={styles.button}
                    onPress={() => {
                        navigation.navigate('Medecin');
                    }
                    }
                >
                    <Text style={{ color: 'white' }}>Rechercher un praticien</Text>
                </TouchableOpacity>

                <Text> {"\n"}</Text>

                <TouchableOpacity style={styles.button}
                    onPress={() => {
                        navigation.navigate('Specialite');
                    }
                    }
                >
                    <Text style={{ color: 'white' }}>Afficher tous les praticiens selon la spécialité</Text>
                </TouchableOpacity>

                <Text> {"\n"}</Text>
                <Text style={styles.textBasique}>
                    Ici vous trouverez les
                <Text style={styles.gras}> horaires</Text>🕐, les
                <Text style={styles.gras}> numéros de téléphone</Text>☎️ et les
                <Text style={styles.gras}> adresses</Text>🏣 des : {"\n"}{"\n"} médecins généralistes, sages-femmes, dentistes, podologues, kinésithérapeutes{"\n"}</Text>


            </View>
        </ImageBackground>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#68656e',
        padding: 10,
        borderRadius: 5,
    },
    gras: {
        fontWeight: "bold",
    },
    logoBruz: {
        width: 40,
        height: 40,
        padding: 10,
        margin: 5,
        resizeMode: 'stretch',
    },
    titre: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: "center",
    },
    textLieu: {
        fontSize: 24,
        textAlign: "center",
    },
    textBasique: {
        fontSize: 14,
        textAlign: "center"
    }


});


export default HomeScreen;
